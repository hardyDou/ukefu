package com.ukefu.util.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.corundumstudio.socketio.SocketIOClient;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ukefu.util.UKTools;

public abstract class NettyClient {
	
	protected Multimap<String, SocketIOClient> clientsMap = HashMultimap.create();
	
	public int size() {
		return clientsMap.size() ;
	}
	
	public Collection<SocketIOClient> getClients(String key){
		return clientsMap.get(key) ;
	}
	
	public Collection<SocketIOClient> getClients() {
		return clientsMap.values() ;
	}
	
	public void putClient(String key , SocketIOClient client) {
		clientsMap.put(key, client) ;
	}
	
	public void clean() {
		List<String> keys = new ArrayList<String>();
		keys.addAll(clientsMap.keySet()) ;
		for(String key : keys) {
			List<SocketIOClient> clients = new ArrayList<SocketIOClient>();
			clients.addAll(this.getClients(key));
			for(SocketIOClient client : clients) {
				if(!client.isChannelOpen()) {
					clientsMap.remove(key, client) ;
				}
			}
		}
	}
	
	public void removeClient(String key , String id) {
		if(this.getClients(key)!=null) {
			List<SocketIOClient> keyClients = new ArrayList<SocketIOClient>();
			keyClients.addAll(this.getClients(key)) ;
			for(int i=0 ; i<keyClients.size() ; i++){
				SocketIOClient client = keyClients.get(i) ;
				if(client!=null) {
					if(UKTools.getContextID(client.getSessionId().toString()).equals(id)){
						clientsMap.remove(key, client) ;
					}
					client.disconnect();
					clientsMap.remove(key, client) ;
				}
			}
		}
	}
}
