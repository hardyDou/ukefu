package com.ukefu.webim.service.sns;

import com.ukefu.core.UKDataContext;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.SNSAccountRepository;
import com.ukefu.webim.web.model.SNSAccount;

public class SnsAccountUtil {
	/**
	 * 获取 渠道名称
	 * @param appid
	 * @param orgi
	 * @return
	 */
	public static SNSAccount process(String appid ,String orgi) {
		SNSAccount account = null ; 
		if((account = (SNSAccount) CacheHelper.getSystemCacheBean().getCacheObject(SnsAccountUtil.getKey(appid), orgi))==null) {
			SNSAccountRepository snsAccountRes = UKDataContext.getContext().getBean(SNSAccountRepository.class) ;
			account = snsAccountRes.findBySnsidAndOrgi(SnsAccountUtil.getKey(appid), orgi) ;
			if(account!=null) {
				CacheHelper.getSystemCacheBean().put(SnsAccountUtil.getKey(appid) , account , orgi) ;
			}
		}
		return account;
	}
	
	public static String getKey(String appid) {
		return appid +"_SNSAccount" ;
	}
}
