package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;

import com.ukefu.util.UKTools;
import com.ukefu.util.event.UserEvent;
import com.ukefu.util.task.ESData;

@Document(indexName = "uckefu", type = "uk_xiaoe_mround_question")
public class MRoundQuestion implements java.io.Serializable ,UserEvent ,ESData{

	/**
	 * 问卷问题表
	 */
	private static final long serialVersionUID = 1115593425069549681L;
	
	private String id = UKTools.getUUID();
	private String title;    //问题标题
	private String name;//问题名称
	private String sortindex;//问题序号
	
	private String mtype ;		//多轮对话类型
	
	
	@Field(index = FieldIndex.not_analyzed)
	private String parentid ;
	
	@Field(index = FieldIndex.not_analyzed)
	private String dataid ;		//多轮对话的 主题ID （来源于场景和来源于 知识库）
	
	@Field(index = FieldIndex.not_analyzed)
	private int quetype;//问题类型（选择题0/问答题1/结束2/转接3）
	
	private String trans;//转接号码
	private boolean interrupt;//打断
	private int interrupttime;//打断开始时间
	private int maxspreak;//最大说话时长
	
	@Field(index = FieldIndex.not_analyzed)
	private String orgi;//租户ID
	
	@Field(index = FieldIndex.not_analyzed)
	private String creater;//创建人
	
	private Date createtime;//创建时间
	private Date updatetime;//更新时间
	
	private String delay ;	//延迟时间
	
	private String content ;
	
	private int offsetx;//位置x
	private int offsety;//位置y
	private String description;//描述
	private String memo;//备注
	private int score;//问题分值
	@Field(index = FieldIndex.not_analyzed)
	private String processid;//问卷ID
	@Field(index = FieldIndex.not_analyzed)
	private String wvtype;//类型（文字/语音）
	
	@Field(index = FieldIndex.not_analyzed)
	private String quevoice;//问题语音ID
	
	private String confirmtype;//答案确认语类型
	private String confirmword;//答案确认语文字
	private String confirmvoice;//答案确认语语音
	
	private String overtimetype;//回答超时语
	private String overtimeword;//回答超时语文字
	private String overtimevoice;//回答超时语语音
	
	private String errortype;//回答错误语
	private String errorword;//回答错误语文字
	private String errorvoice;//回答错误语语音
	
	private String replykeyword ;	//遇到设置的关键词后重复播放语音
	private String replytype;//重复提示类型
	private String replyword;//重复提示语文字
	private String replyvoice;//重复提示语语音
	
	private String replyrepeat;//重复确认语-最大重复次数
	private String replyoperate;//重复确认语-到达最大次数的操作（转接trans/挂断/handup）
	private String replytrans;//重复确认语-转接号码
	private String replytypeup;//重复确认语-挂断提示语类型
	private String replywordup;//重复确认语-挂断提示语（文字）
	private String replyvoiceup;//重复确认语-挂断提示语（语音ID）
	
	private String overtime ;	//超时时长
	private String overtimerepeat;//回答超时语-最大重复次数
	private String overtimeoperate;//回答超时语-到达最大次数的操作（转接trans/挂断/handup）
	private String overtimetrans;//回答超时语-转接号码
	private String overtimetypeup;//回答超时语-挂断提示语类型
	private String overtimewordup;//回答超时语-挂断提示语（文字）
	private String overtimevoiceup;//回答超时语-挂断提示语（语音ID）
	
	private String errorepeat;//回答错误语-最大重复次数
	private String erroroperate;//回答错误语-到达最大次数的操作（转接trans/挂断/handup）
	private String errortrans;//回答错误语-转接号码
	private String errortypeup;//回答错误语-挂断提示语类型
	private String errorwordup;//回答错误语-挂断提示语（文字）
	private String errorvoiceup;//回答错误语-挂断提示语（语音ID）
	
	
	private String userask;//回答错误语
	private String askword;//回答错误语文字
	
	private String endbyai ;	//结束会话
	
	private boolean staticagent ;	//启用无感知转人工
	private boolean bussop ;		//触发业务操作
	private String busslist ;		//触发的业务操作列表
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSortindex() {
		return sortindex;
	}
	public void setSortindex(String sortindex) {
		this.sortindex = sortindex;
	}
	
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public int getQuetype() {
		return quetype;
	}
	public void setQuetype(int quetype) {
		this.quetype = quetype;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getProcessid() {
		return processid;
	}
	public void setProcessid(String processid) {
		this.processid = processid;
	}
	
	public String getWvtype() {
		return wvtype;
	}
	public void setWvtype(String wvtype) {
		this.wvtype = wvtype;
	}
	public String getQuevoice() {
		return quevoice;
	}
	public void setQuevoice(String quevoice) {
		this.quevoice = quevoice;
	}
	public String getConfirmtype() {
		return confirmtype;
	}
	public void setConfirmtype(String confirmtype) {
		this.confirmtype = confirmtype;
	}
	public String getConfirmword() {
		return confirmword;
	}
	public void setConfirmword(String confirmword) {
		this.confirmword = confirmword;
	}
	public String getConfirmvoice() {
		return confirmvoice;
	}
	public void setConfirmvoice(String confirmvoice) {
		this.confirmvoice = confirmvoice;
	}
	public String getOvertimetype() {
		return overtimetype;
	}
	public void setOvertimetype(String overtimetype) {
		this.overtimetype = overtimetype;
	}
	public String getOvertimeword() {
		return overtimeword;
	}
	public void setOvertimeword(String overtimeword) {
		this.overtimeword = overtimeword;
	}
	public String getOvertimevoice() {
		return overtimevoice;
	}
	public void setOvertimevoice(String overtimevoice) {
		this.overtimevoice = overtimevoice;
	}
	public String getErrortype() {
		return errortype;
	}
	public void setErrortype(String errortype) {
		this.errortype = errortype;
	}
	public String getErrorword() {
		return errorword;
	}
	public void setErrorword(String errorword) {
		this.errorword = errorword;
	}
	public String getErrorvoice() {
		return errorvoice;
	}
	public void setErrorvoice(String errorvoice) {
		this.errorvoice = errorvoice;
	}
	public String getReplykeyword() {
		return replykeyword;
	}
	public void setReplykeyword(String replykeyword) {
		this.replykeyword = replykeyword;
	}
	public String getReplytype() {
		return replytype;
	}
	public void setReplytype(String replytype) {
		this.replytype = replytype;
	}
	public String getReplyword() {
		return replyword;
	}
	public void setReplyword(String replyword) {
		this.replyword = replyword;
	}
	public String getReplyvoice() {
		return replyvoice;
	}
	public void setReplyvoice(String replyvoice) {
		this.replyvoice = replyvoice;
	}
	public String getReplyrepeat() {
		return replyrepeat;
	}
	public void setReplyrepeat(String replyrepeat) {
		this.replyrepeat = replyrepeat;
	}
	public String getReplyoperate() {
		return replyoperate;
	}
	public void setReplyoperate(String replyoperate) {
		this.replyoperate = replyoperate;
	}
	public String getReplytrans() {
		return replytrans;
	}
	public void setReplytrans(String replytrans) {
		this.replytrans = replytrans;
	}
	public String getReplytypeup() {
		return replytypeup;
	}
	public void setReplytypeup(String replytypeup) {
		this.replytypeup = replytypeup;
	}
	public String getReplywordup() {
		return replywordup;
	}
	public void setReplywordup(String replywordup) {
		this.replywordup = replywordup;
	}
	public String getReplyvoiceup() {
		return replyvoiceup;
	}
	public void setReplyvoiceup(String replyvoiceup) {
		this.replyvoiceup = replyvoiceup;
	}
	public String getOvertimerepeat() {
		return overtimerepeat;
	}
	public void setOvertimerepeat(String overtimerepeat) {
		this.overtimerepeat = overtimerepeat;
	}
	public String getOvertimeoperate() {
		return overtimeoperate;
	}
	public void setOvertimeoperate(String overtimeoperate) {
		this.overtimeoperate = overtimeoperate;
	}
	public String getOvertimetrans() {
		return overtimetrans;
	}
	public void setOvertimetrans(String overtimetrans) {
		this.overtimetrans = overtimetrans;
	}
	public String getOvertimetypeup() {
		return overtimetypeup;
	}
	public void setOvertimetypeup(String overtimetypeup) {
		this.overtimetypeup = overtimetypeup;
	}
	public String getOvertimewordup() {
		return overtimewordup;
	}
	public void setOvertimewordup(String overtimewordup) {
		this.overtimewordup = overtimewordup;
	}
	public String getOvertimevoiceup() {
		return overtimevoiceup;
	}
	public void setOvertimevoiceup(String overtimevoiceup) {
		this.overtimevoiceup = overtimevoiceup;
	}
	public String getErrorepeat() {
		return errorepeat;
	}
	public void setErrorepeat(String errorepeat) {
		this.errorepeat = errorepeat;
	}
	public String getErroroperate() {
		return erroroperate;
	}
	public void setErroroperate(String erroroperate) {
		this.erroroperate = erroroperate;
	}
	public String getErrortrans() {
		return errortrans;
	}
	public void setErrortrans(String errortrans) {
		this.errortrans = errortrans;
	}
	public String getErrortypeup() {
		return errortypeup;
	}
	public void setErrortypeup(String errortypeup) {
		this.errortypeup = errortypeup;
	}
	public String getErrorwordup() {
		return errorwordup;
	}
	public void setErrorwordup(String errorwordup) {
		this.errorwordup = errorwordup;
	}
	public String getErrorvoiceup() {
		return errorvoiceup;
	}
	public void setErrorvoiceup(String errorvoiceup) {
		this.errorvoiceup = errorvoiceup;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getOffsetx() {
		return offsetx;
	}
	public void setOffsetx(int offsetx) {
		this.offsetx = offsetx;
	}
	public int getOffsety() {
		return offsety;
	}
	public void setOffsety(int offsety) {
		this.offsety = offsety;
	}
	public String getTrans() {
		return trans;
	}
	public void setTrans(String trans) {
		this.trans = trans;
	}

	public int getMaxspreak() {
		return maxspreak;
	}
	public void setMaxspreak(int maxspreak) {
		this.maxspreak = maxspreak;
	}
	public boolean isInterrupt() {
		return interrupt;
	}
	public void setInterrupt(boolean interrupt) {
		this.interrupt = interrupt;
	}
	public int getInterrupttime() {
		return interrupttime;
	}
	public void setInterrupttime(int interrupttime) {
		this.interrupttime = interrupttime;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUserask() {
		return userask;
	}
	public void setUserask(String userask) {
		this.userask = userask;
	}
	public String getAskword() {
		return askword;
	}
	public void setAskword(String askword) {
		this.askword = askword;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getEndbyai() {
		return endbyai;
	}
	public void setEndbyai(String endbyai) {
		this.endbyai = endbyai;
	}
	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	public String getOvertime() {
		return overtime;
	}
	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}
	public boolean isStaticagent() {
		return staticagent;
	}
	public void setStaticagent(boolean staticagent) {
		this.staticagent = staticagent;
	}
	public boolean isBussop() {
		return bussop;
	}
	public void setBussop(boolean bussop) {
		this.bussop = bussop;
	}
	public String getBusslist() {
		return busslist;
	}
	public void setBusslist(String busslist) {
		this.busslist = busslist;
	}
}
